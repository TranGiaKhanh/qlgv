<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
    protected $table = 'classes';
    public $fillable = [ 'name', 'manager'];

    public function user()
    {
        return $this->hasMany(User::class, "department_id", "id");
    }
}
