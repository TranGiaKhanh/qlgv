<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ClassesRequest;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Classes\ClassesRepositoryInterface;

class ClassesController extends Controller
{
    function __construct(ClassesRepositoryInterface $classes, UserRepositoryInterface $users)
    {
        $this->classes = $classes;
    }

    public function index()
    {
        $classes = $this->classes->getAllClasses();
        return view('classes.list', compact("classes"));
    }

    public function create()
    {
        return view('classes.create');
    }

    public function store(ClassesRequest $request)
    {
        $data = $request->except('_token');
        $this->classes->store($data);
        return redirect()->route('classes.index')->with('success', 'Tạo lớp thành công!');
    }

    public function edit($id)
    {
        $classes = $this->classes->getById($id);
        return view('classes.update', compact('classes'));
    }

    public function update(ClassesRequest $request, $id)
    {
        $data = $request->except("_token");
        $this->classes->update($data, $id);
        return redirect()->route('classes.index')->with('success', 'Sửa khoa thành công!');
    }

    public function destroy($id)
    {
        $this->classes->destroy($id);
        return redirect()->route('classes.index')->with('success', 'Xoá khoa thành công!');
    }
}

