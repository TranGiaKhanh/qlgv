<?php
return[
    'FIRSTLOGIN' => 1,
    'NOTFIRSTLOGIN' => 0,
    'WORKSTATUS' => [
        'Working' => 0,
        'Retired' => 1,
    ],
    'ROLE' => [
        'ADMIN' => 'Admin',
        'MANAGER' => 'Quản lý',
        'EMPLOYEE' => 'Giảng viên',
    ],
    'ITEMS_PERPAGE' => 2,
    'ROLE_EMPLOYEE' => 2,
    'ROLE_MANAGER' => 1,
];
