<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create(['name' => 'Lý luận cơ sở', 'status'=> 'abv']);
        Department::create(['name' => 'Xây dựng đảng', 'status'=> 'ãc']);
        Department::create(['name' => 'Nhà nước và pháp luận', 'status'=> 'zxxxx']);
    }
}
