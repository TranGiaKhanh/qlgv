@extends('layouts.master')
@section('title')
    Danh sách khoa
@endsection
@section('content')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Cập nhập</h4>
                <form action="{{ route('departments.update', $departments->id) }}" class="forms-sample" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputDepartmentname">Tên khoa <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" value="{{ $departments->name }}">
                        @error('name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDescription">Trạng thái hoạt động</label>
                        <select class="form-control" name="status" placeholder="{{ $departments->status}}">
                                        <option value="Đang hoạt động">Đang hoạt động</option>
                                        <option value="Tạm ngưng">Tạm ngưng</option>
                                    </select>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button type="button" onclick="window.location.href='{{ route('departments.index') }}'"
                        class="btn btn-light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
@endsection
