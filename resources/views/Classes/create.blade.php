@extends('layouts.master')
@section('title')
    Thêm
@endsection
@section('content')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Thêm Lớp</h4>
                <form action="{{ route('classes.store') }}" class="forms-sample" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputClassesname">Tên <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" >
                        @error('name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputManager">Chủ nhiệm <span class="text-danger" >*</span></label>
                        <input type="text" name='manager' class='form-control'>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button type="button" onclick="window.location.href='{{ route('classes.index') }}'"
                        class="btn btn-light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
@endsection
