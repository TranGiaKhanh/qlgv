@extends('layouts.master')
@section('title')
    Dánh sách lớp
@endsection
@section('content')
    <div class="col-lg-12 grid-margin stretch-card background-color-grey">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Dánh sách lớp</h4>
                <p class="card-description">
                </p>
                @if (session()->has('success'))
                    <h3 class="alert alert-success">
                        {{ session()->get('success') }}
                    </h3>
                @endif
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tên lớp</th>
                            <th>Chủ nhiệm</th>
                            <th>Create at</th>
                            <th>Update at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($classes))
                            @foreach ($classes as $key => $classes)
                                <tr>
                                    <td class="py-1">
                                        {{ $key + 1 }}
                                    </td>
                                    <td>
                                        {{ $classes->name }}
                                    </td>
                                    <td>
                                        {{ $classes->manager }}
                                    </td>
                                    {{-- <td>
                                        <!-- @foreach ($department->user as $manager)
                                            {{ $manager->name . '. ' }}
                                        @endforeach -->
                                    </td> --}}
                                    <td>
                                        {{ $classes->created_at->format("d/m/Y") }}
                                    </td>
                                    <td>
                                        {{ $classes->updated_at->format("d/m/Y") }}
                                    </td>
                                    <td>
                                        <a href="{{ route('classes.edit', $classes->id) }}"
                                            class="btn btn-primary mr-2">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-danger" href="#" data-toggle="modal"
                                            data-target="#ModalDelete{{ $classes->id }}">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                        <form id="delete_form_{{ $classes->id }}" method="get"
                                            action="{{ route('classes.destroy', $classes->id) }}"
                                            style="display:none">
                                            @csrf
                                        </form>
                                    </td>
                                    @include('modals.delete-classes')
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="12">Không tìm thấy trang</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {{-- {{ $classes->links() }} --}}
            </div>
        </div>
    </div>
@endsection
