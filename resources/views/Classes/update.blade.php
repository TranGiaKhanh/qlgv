@extends('layouts.master')
@section('title')
    Danh sách Lớp
@endsection
@section('content')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Cập nhập</h4>
                <form action="{{route('classes.update', $classes->id) }}" class="forms-sample" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputClassesname">Tên khoa <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" value="{{ $classes->name }}">
                        @error('name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputManager">Chủ nhiệm <span class="text-danger">*</label>
                        <input type="text" name="manager" class="form-control" value="{{ $classes->manager }}">
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button type="button" onclick="window.location.href='{{ route('classes.index') }}'"
                        class="btn btn-light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
@endsection
