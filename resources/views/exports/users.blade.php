<h4>List User</h4>
<br>
<div>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Image</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Birthday</th>
                <th>Work Day</th>
                <th>Department</th>
                <th>Role</th>
                <th>Work Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $key => $user)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $user->image }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->address }}</td>
                    <td>{{ date('Y-m-d', strtotime($user->birth_day)) }}</td>
                    <td>{{ date('Y-m-d', strtotime($user->work_at)) }}</td>
                    <td>{{ $user->department->name }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ __('message.WORKSTATUS.' . $user->work_status) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
